
CREATE TABLE `campaign_email_solo_group` (
  `campaign_email_solo_group_id` int(11) NOT NULL,
  `group_name` varchar(45) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
