
CREATE TABLE `user_deposit` (
  `user_deposit_id` int(11) NOT NULL,
  `trans_id` varchar(15) NOT NULL,
  `ifxaccount_id` int(11) NOT NULL,
  `exchange_rate` decimal(10,2) NOT NULL,
  `dollar_ordered` decimal(10,2) NOT NULL,
  `naira_equivalent_dollar_ordered` decimal(10,2) NOT NULL,
  `naira_service_charge` decimal(10,2) NOT NULL,
  `naira_vat_charge` decimal(10,2) NOT NULL,
  `naira_stamp_duty` decimal(10,2) NOT NULL,
  `naira_total_payable` decimal(10,2) NOT NULL,
  `client_naira_notified` decimal(10,2) DEFAULT NULL,
  `client_pay_date` date DEFAULT NULL,
  `client_pay_method` enum('1','2','3','4','5','6','7','8','9') DEFAULT NULL COMMENT '1 - WebPay 2 - Internet Transfer 3 - ATM Transfer 4 - Bank Transfer 5 - Mobile Money Transfer 6 - Cash Deposit 7 - Office Funding 8 - Not Listed 9 - USSD',
  `client_reference` varchar(255) DEFAULT NULL,
  `client_comment` varchar(255) DEFAULT NULL,
  `client_comment_response` enum('1','2') DEFAULT '2' COMMENT '1 - Yes\n2 - No',
  `client_notified_date` datetime DEFAULT NULL,
  `real_naira_confirmed` decimal(10,2) DEFAULT NULL,
  `real_dollar_equivalent` decimal(10,2) DEFAULT NULL,
  `points_claimed_id` int(11) DEFAULT NULL,
  `transfer_reference` text COMMENT 'Deposit Transaction Reference Details From Instaforex After Completing The Transaction',
  `deposit_origin` enum('1','2','3') DEFAULT '1' COMMENT '1 - Online\n2 - Diamond Office\n3 - Ikota Office',
  `status` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '1' COMMENT '1 - Deposit Initiated\n2 - Notified\n3 - Confirmation In Progress\n4 - Confirmation Declined\n5 - Confirmed\n6 - Funding In Progress\n7 - Funding Declined\n8 - Funded / Completed\n9 - Payment Failed\n10 - Expired',
  `order_complete_time` timestamp NULL DEFAULT NULL COMMENT 'Time order status was changed to complete',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
