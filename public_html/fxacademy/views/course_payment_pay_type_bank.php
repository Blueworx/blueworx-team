<ul class="fa-ul">
    <li>Order Submitted</li>
</ul>
<p>Your transaction details has been submitted. Please proceed to make payment/transfer into the account
details below.</p>
<ol>
    <li>Any Branch of Guaranty Trust Bank<br />
        Account Name: Instant Web-Net Technologies Ltd<br />
        Account Number: 0153738149</li>
    <li>Please include your Transaction ID <strong>(<?php echo $trans_id; ?>)</strong>.</li>
</ol>
<p>After payment, please call 08028281192 or email support@instafxng.com for payment notification.</p>