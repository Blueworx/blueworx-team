        <!--<base href="https://instafxng.com/admin/" />-->
        <meta http-equiv="Content-Language" content="en" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="Instant Web-Net Technologies Limited" />
        <meta name="publisher" content="Instant Web-Net Technologies Limited" />
        <meta name="copyright" content="Instant Web-Net Technologies Limited" />
        <meta name="rating" content="General" />
        <meta name="doc-rights" content="Private" />
        <meta name="doc-class" content="Living Document" />
        <link rel="stylesheet" href="../css/instafx_admin.css">
        <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.png">
        <link rel="stylesheet" href="../css/bootstrap_3.3.5.min.css">
        <link rel="stylesheet" href="../css/font-awesome_4.6.3.min.css">
        <link href="../css/bootstrap-datetimepicker.css" rel="stylesheet">
        <script src="../js/jquery_2.1.1.min.js"></script>
        <script src="../js/bootstrap_3.3.5.min.js"></script>

        <!--        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
        <!--        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
        <!--        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->


        <script src="../js/ie10-viewport-bug-workaround.js"></script>
        <script src="../js/validator.min.js"></script>
        <script src="../js/npm.js"></script>
        <script src="../js/Chart.min.js"></script>
        <script src="../js/instafx_admin.js"></script>


        <!-- NOTIFICATION CSS -->
        <style>
                .modal.fade .modal-dialog
                {
                        transform: translate(0px, -25%);
                        transition: transform 0.3s ease-out 0s;
                }
                .modal.fade.in .modal-dialog
                {
                        transform: translate(0px, 0px);
                }

                .flyover
                {
                        left: 150%;
                        overflow: hidden;
                        position: fixed;
                        width: 50%;
                        opacity: 0.95;
                        z-index: 1050;
                        transition: left 0.6s ease-out 0s;
                }

                .flyover-centered {
                        top: 50%;
                        transform: translate(-50%, -50%);
                }

                .flyover.in {
                        left: 50%;
                }
        </style>
        <!-- NOTIFICATION CSS -->