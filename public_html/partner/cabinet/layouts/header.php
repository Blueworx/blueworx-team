
        <!-- Header Section: Logo and Live Chat  -->
        <header id="header">
            <div class="container-fluid no-gutter masthead">
                <div class="row">
                    <div id="main-logo" class="col-sm-12 col-md-9">
                        <a href="partner/cabinet/" title="Home Page"><img src="images/ifxlogo.png?v=1.1" alt="Instaforex Nigeria Logo" /></a>
                    </div>
                    <div id="top-nav" class="col-sm-12 col-md-3" style="margin-top: 8px;">
                        <div style="font-size: 1.4em;">
                            <a href="" title=""><span class="badge">0</span></a>
                            <a href="#" title=""><i class="fa fa-user fa-fw"></i></a>&nbsp;&nbsp;
                            <a href="partner/cabinet/logout.php" title="Log Out"><i class="fa fa-sign-out fa-fw"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </header>