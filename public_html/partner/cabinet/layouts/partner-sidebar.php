<!-- Main Body - Side Bar  -->
<div class="col-md-12">
    <div class="list-group" style="margin-bottom: 5px !important;">
        <a class="list-group-item" href="" title="" style=" background-color: #373739; color: #FFFFFF">
            <p class="text-capitalize text-center"><strong>Partner Full Name</strong></p>
        </a>
    </div>
</div>

<div class="col-md-12">
    <nav id="side-nav" class="navbar navbar-default" role="navigation">
         <!--Brand and toggle get grouped for better mobile display--> 
        <div class="navbar-header">
            <span class="visible-xs navbar-brand">Menu <i class="fa fa-fw fa-long-arrow-right"></i></span>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

         <!--Collect the nav links, forms, and other content for toggling--> 
        <div class="collapse navbar-collapse navbar-ex1-collapse left-navigation">
            <ul class="nav navbar-nav">
                <li><a href="./" title="Partner Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars fa-fw"></i> Admin<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="my_account.php" title="My profile">Profile</a></li>
                        <li><a href="referrals_listing.php" title="My Referrals">My Referrals</a></li>
                        <li><a href="resources.php" title="Partner Resources">Partner Resources</a></li>
                        <li><a href="change_password.php" title="Change password">Change Password</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars fa-fw"></i> Transactions<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="commission.php" title="Commissions">Commissions</a></li>
                        <li><a href="withdraw_history.php" title="Withdrawal History">Withdrawal History</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars fa-fw"></i> Help Centre<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="help.php" title="Customer support">Customer Support</a></li>
                        <li><a href="ipp_terms.php" title="Instafxng Partners Program">IPP Terms</a></li>
                        <li><a href="about_ipp.php" title="About IPP">About IPP</a></li>
                    </ul>
                </li>
                <li><a href="logout.php" title="Log Out"><i class="fa fa-sign-out fa-fw"></i> Log Out</a></li>
            </ul>
        </div>
    </nav>
</div>

<div class="col-md-12">
    <div class="nav-display super-shadow">
        <header><i class="fa fa-bars fa-fw"></i> Connect With Us</header>
        <article>
            <a href="https://facebook.com/InstaForexNigeria" target="_blank"><img src="../images/Facebook.png"></a>
            <a href="https://twitter.com/Instafxng" target="_blank"><img src="../images/Twitter.png"></a>
            <a href="https://linkedin.com/company/instaforex-ng" target="_blank"><img src="../images/LinkedIn.png"></a>
        </article>
    </div>
</div>