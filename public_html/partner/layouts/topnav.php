       
        <!-- Top Navigation: The main navigation of the Web site  -->
        <nav id="top-nav" class="navbar navbar-inverse">
            <div class="navbar-header">
                <span class="visible-xs navbar-brand">Menu <i class="fa fa-fw fa-long-arrow-right"></i></span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div  class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="partner/index.php" title="Home Page"><i title="Instafxng Partners" class="fa fa-home"></i> </a></li>
                    <li><a href="partner/how_it_works.php" title="How it Works"><i class="fa fa-file-text-o fa-fw"></i> How it Works</a></li>
                    <li><a href="partner/signup.php" title="How it Works"><i class="fa fa-pencil-square-o fa-fw"></i> Sign Up</a></li>
                    <li><a href="partner/banners.php" title="Banners"><i class="fa fa-tasks fa-fw"></i> Banners</a></li>
                    <li><a href="partner/ways_to_earn.php" title="Ways to Earn"><i class="fa fa-money fa-fw"></i> Ways to Earn</a></li>
                    <li><a href="https://instafxng.com/contact_info.php" target="_blank" title="Contact us"><i class="fa fa-phone fa-fw"></i> Contact us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="partner/login.php" title="Instafxng Partner Login"><i class="fa fa-sign-in fa-fw"></i> Instafxng Partner Login</a></li>
                </ul>
            </div>
        </nav>
        