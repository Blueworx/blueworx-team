<p>Welcome to our partnership program.</p>
<ul class="fa-ul">
    <li><i class="fa-li fa fa-check-square-o icon-tune"></i>Enter your email address below and click the submit button
        to begin your registration. It takes less than 5 minutes.</li>
</ul>

<form data-toggle="validator" class="form-horizontal" role="form" method="post" action="">
    <div class="form-group">
        <label class="control-label col-sm-3" for="your_email">Email Address:</label>
        <div class="col-sm-9 col-lg-5">
            <input name="your_email" type="text" class="form-control" id="your_email" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9"><input name="signup_request_email" type="submit" class="btn btn-success" value="Submit" /></div>
    </div>
</form>
<p class="text-center">Already have a Partnership Account? <a href="partner/login.php" title="Log in to your IPP">Login here</a></p>