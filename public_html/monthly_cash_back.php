<?php
require_once 'init/initialize_general.php';
$thisPage = "Promotion";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <base target="_self">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Instaforex Nigeria | Monthly Cash Back Promotion</title>
        <meta name="title" content="Instaforex Nigeria | Monthly Cash Back Promotion" />
        <meta name="keywords" content="instaforex, forex promotions, forex monthly cash back, instaforex nigeria." />
        <meta name="description" content="Meet Certain Trading conditions and get $50 funded back to your trading account monthly, details here" />
        <?php require_once 'layouts/head_meta.php'; ?>
    </head>
    <body>
        <?php require_once 'layouts/header.php'; ?>
        <!-- Main Body: The is the main content area of the web site, contains a side bar  -->
        <div id="main-body" class="container-fluid">
            <div class="row no-gutter">
                <?php require_once 'layouts/topnav.php'; ?>
                <!-- Main Body - Content Area: This is the main content area, unique for each page  -->
                <div id="main-body-content-area" class="col-md-8 col-md-push-4 col-lg-9 col-lg-push-3">
                    
                    <!-- Unique Page Content Starts Here
                    ================================================== -->
                    
                    <div class="section-tint super-shadow">
                        <div class="row">
                            <div class="col-sm-12 text-danger">
                                <h4><strong>Monthly Cash Back Promotion</strong></h4>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-danger"><strong>This promotion has been replaced with "Instafxng Point Based Loyalty Program and Rewards".
                                    You have a chance to Make Up To $4, 200 and N1, 000, 000 Extra While You Take Your Normal Trades.</strong>
                                    <a href="loyalty.php" class="btn btn-success">Click Here for Full Details</a>
                                </p>
                                <p>InstaForex Nigeria offers a Cash Back award of $50 to each trader account which has recorded trades of a minimum of 100 Insta lots in a calendar month.</p>
                                <p>At the end of each month, during the first week of the following month, the winners (which shall be every account that has traded at least 100 lots) will be 
                                listed on this page and they will be credited with their winning of $50 each.</p>
                                <p><strong>Conditions For Participation</strong></p>
                                <ul class="fa-ul">
                                    <li><i class="fa-li fa fa-check-square-o icon-tune"></i>To be eligible for the contest, your trading account must be enrolled in the InstaFxNg Loyalty Program and Rewards (ILPR). 
                                        Please confirm with our <a href="contact_info.php" target="_blank" title="Confirm that you have a qualifying account">support department</a> that your InstaForex Account qualifies.
                                        If you don't have an InstaForex Account yet, <a href="live_account.php" target="_blank" title="Open A Live Trading Account">open a qualifying account</a> now.
                                    </li>
                                    <li><i class="fa-li fa fa-check-square-o icon-tune"></i>Trade a minimum of 100 Lots in a month and qualify to receive the Cash Back award.</li>
                                    <li><i class="fa-li fa fa-check-square-o icon-tune"></i>Cent accounts will have to trade 10,000 lots to qualify.</li>
                                </ul>
                            </div>
                            
                            <div class="col-sm-12">
                                <h5><strong>Clients Credited for October, 2016</strong></h5>
                                <table class="table table-responsive table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Client Name</th>
                                            <th>Account Number</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>Loveday Ibeanu</td><td>1489083</td></tr>
                                        <tr><td>Chukwuka Chidiebere</td><td>1511087</td></tr>
                                        <tr><td>Ogunsanwo Olusegun</td><td>2425737</td></tr>
                                        <tr><td>Joseph Ajayi</td><td>2449627</td></tr>
                                        <tr><td>Zakariya Usman</td><td>2462854</td></tr>
                                        <tr><td>Uwafili Anthony</td><td>2462858</td></tr>
                                        <tr><td>Apata Babatunde</td><td>5299852</td></tr>
                                        <tr><td>Oluwaseyi Fowobaje</td><td>5341284</td></tr>
                                        <tr><td>Bello Akanbi</td><td>5486780</td></tr>
                                        <tr><td>Oluokun Adeyemi</td><td>7593209</td></tr>
                                        <tr><td>Obinna Nwosu</td><td>8657528</td></tr>
                                        <tr><td>Ogundare Ajayi</td><td>8719119</td></tr>
                                        <tr><td>Nwatu Ogadimma</td><td>8741224</td></tr>
                                        <tr><td>Woyinkuro Amungo</td><td>8771407</td></tr>
                                        <tr><td>Gabriel Iduh</td><td>8772436</td></tr>
                                        <tr><td>Agbeleye Samuel</td><td>8855638</td></tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <!-- Unique Page Content Ends Here
                    ================================================== -->
                    
                </div>
                <!-- Main Body - Side Bar  -->
                <div id="main-body-side-bar" class="col-md-4 col-md-pull-8 col-lg-3 col-lg-pull-9 left-nav">
                <?php require_once 'layouts/sidebar.php'; ?>
                </div>
            </div>
        </div>
        <?php require_once 'layouts/footer.php'; ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>