<?php
require_once 'init/initialize_general.php';
$thisPage = "Education";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <base target="_self">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Instaforex Nigeria | 100% Bonus for New Clients</title>
        <meta name="title" content="Instaforex Nigeria | 100% Bonus for New Clients" />
        <meta name="keywords" content="instaforex education bonus, instaforex, forex trading in nigeria, forex seminar, forex trading seminar, how to trade forex, trade forex, instaforex nigeria">
        <meta name="description" content="Instaforex, a multiple award winning international forex broker offers an 100% Education Bonus for those who have participated in our Forex Trading Training.">
        <?php require_once 'layouts/head_meta.php'; ?>
    </head>
    <body>
        <?php require_once 'layouts/header.php'; ?>
        <!-- Main Body: The is the main content area of the web site, contains a side bar  -->
        <div id="main-body" class="container-fluid">
            <div class="row no-gutter">
                <?php require_once 'layouts/topnav.php'; ?>
                <!-- Main Body - Content Area: This is the main content area, unique for each page  -->
                <div id="main-body-content-area" class="col-md-8 col-md-push-4 col-lg-9 col-lg-push-3">
                    
                    <!-- Unique Page Content Starts Here
                    ================================================== -->
                    <div class="super-shadow page-top-section">
                        <div class="row ">
                            <div class="col-sm-6">
                                <h2>Instaforex 100% Bonus for New Clients</h2>
                                <p>Instaforex offers a unique 100% Forex bonus to all new clients who recently joined
                                    the company.</p>
                                <p>This bonus is to increase your capital and give you an opportunity to earn
                                more in the Forex market.</p>
                            </div>

                            <div class="col-sm-6">
                                <img src="images/instaforex-bonus.jpg" alt="" class="img-responsive" />
                            </div>
                        </div>
                    </div>

                    <div class="section-tint super-shadow">
                        <h5>Please follow the steps below to claim your 100% Forex Bonus.</h5>
                        <ol>
                            <li>To  get the 100% bonus, you must have funded your account with either $50, $100, $150 or even $1000.
                            <a href="deposit.php" title="Fund your Instaforex Account" target="_blank">Click here</a> to fund your account now.</li>
                            <li>Download the bonus application form and fill the column for name, email address, account number and signature
                                only. Leave the other column unfilled. <a href="downloads/instaforex_education_bonus_form.jpg" title="Click to download bonus application form" download>Click here to download form</a></li>
                            <li>Scan and send the form alongside a valid government issued ID card such as International passport, National ID card,
                                Tax card, Permanent voters card, driver’s license to support@instafxng.com</li>
                            <p>After receiving your scanned application and ID card, The bonus will be processed within 24 hours.
                                DO NOT PLACE A TRADE ON THE ACCOUNT till the bonus has been processed.</p>
                        </ol>
                        <p>For further enquiries, please call 08182045184, 08028281192, 08083956750 or send a mail to support@instafxng.com</p>
                    </div>

                    <!-- Unique Page Content Ends Here
                    ================================================== -->

                </div>
                <!-- Main Body - Side Bar  -->
                <div id="main-body-side-bar" class="col-md-4 col-md-pull-8 col-lg-3 col-lg-pull-9 left-nav">
                <?php require_once 'layouts/sidebar.php'; ?>
                </div>
                
            </div>
        </div>
        <?php require_once 'layouts/footer.php'; ?>
    </body>
</html>