<?php
require_once 'init/initialize_general.php';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <base target="_self">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Instaforex Nigeria | NIGERIA @ 57</title>
        <meta name="title" content="Instaforex Nigeria | NIGERIA @ 57" />
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta http-equiv="Content-Language" content="en" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta name="robots" content="index, follow" />
        <meta name="author" content="Instant Web-Net Technologies Limited" />
        <meta name="publisher" content="Instant Web-Net Technologies Limited" />
        <meta name="copyright" content="Instant Web-Net Technologies Limited" />
        <meta name="rating" content="General" />
        <meta name="doc-rights" content="Private" />
        <meta name="doc-class" content="Living Document" />
        <link rel="stylesheet" href="forex-income/css/instafx_fi.css">
        <link rel="stylesheet" href="css/free_seminar.css">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/validator.min.js"></script>
        <script src="js/npm.js"></script>
    </head>
    <body>
        <!-- Header Section: Logo and Live Chat  -->
        <header id="header">
            <div class="container-fluid no-gutter masthead">
                <div class="row">
                    <div id="main-logo" class="col-sm-12 col-md-5">
                        <img src="images/ifxlogo.png" alt="Instaforex Nigeria Logo" />
                    </div>
                    <div id="top-nav" class="col-sm-12 col-md-7 text-right">
                    </div>
                </div>
            </div>
            <hr />
        </header>

        <!-- Main Body: The is the main content area of the web site, contains a side bar  -->
        <div id="main-body" class="container-fluid">
            <div class="row no-gutter">

                <!-- Main Body - Content Area: This is the main content area, unique for each page  -->
                <div id="main-body-content-area" class="col-md-12">

                    <!-- Unique Page Content Starts Here
                    ================================================== -->
                    <div class="section-tint super-shadow">
                        <div class="row">
                            <div class="col-sm-12">
                                <img src="images/independence-banner-1.jpg" alt="" class="img-responsive center-block" />
                                <hr />
                            </div>
                        </div>

                        <div class="row text-center">
                            <div class="col-sm-12">
                                <?php require_once 'layouts/feedback_message.php'; ?>
                                <h3 class="text-success"><strong>NIGERIA @ 57</strong></h3>
                                <p><strong>Participate in the Instafxng Independence Day Contest to <span class="text-success">WIN $20</span></strong></p>
                            </div>
                        </div>
                        <hr />


                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-justify">How about participating in a contest that gets you $20 to commemorate this year�s Independence
                                    celebration?</p>
                                <p class="text-justify">Cool right? Yes!</p>
                                <p class="text-justify">You can be one of the 5 clients to be rewarded with $20 each in the ongoing Independence Day Contest
                                    just by showing us how well you know this beautiful country of ours � Nigeria.</p>
                                <p class="text-justify">Yes! Your knowledge of the history of Nigeria is needed here to for you to get the $20.</p>
                                <p class="text-justify">Follow the rules and regulations of the contest below diligently and attempt all questions
                                    to the best of your knowledge.</p>
                                <p class="text-justify">Sure you want to win $20? Then let’s get to work!</p>
                                <p class="text-justify">This contest will run from today 1st of October to the 3rd of October 2017.</p>
                            </div>
                        </div>

                        <div class="section-tint-blue super-shadow">
                            <p class="text-justify">Follow the rules and regulations of the contest below diligently and
                                attempt all questions to the best of your knowledge.</p>
                            <ol>
                                <li>Only accounts enrolled on Instafxng Loyalty Program and Rewards (ILPR) can participate in this contest.</li>
                                <li>You have 1 minute 15 secs to answer 15 multiple choice questions on the history of Nigeria.
                                    Each question has a time allotted it.</li>
                                <li>If you couldn’t select an answer before time out, you have missed the question.</li>
                                <li> You are allowed to participate in this contest just once even if you have more than one account.</li>
                                <li>Five people with the highest scores will get $20 each.</li>
                            </ol>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <center><a href="independence-quiz/"><button class="btn btn-lg btn-success">Are you still there? Click this button to get started now!</button></a></center>
                            </div>
                        </div>

                        <hr />
                        <div class="row text-center">
                            <br />
                            <p class="color-fancy">For further enquiries, please call 08182045184, 07081036115</p>
                        </div>

                    </div>

                    <!-- Unique Page Content Ends Here
                    ================================================== -->
                </div>
            </div>


        </div>
        <footer id="footer" class="super-shadow">
            <div class="container-fluid no-gutter">
                <div class="col-sm-12">
                    <div class="row">
                        <p class="text-center" style="font-size: 16px !important;">&copy; <?php echo date('Y'); ?>, All rights reserved. Instant Web-Net Technologies Limited (www.instafxng.com)</p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>